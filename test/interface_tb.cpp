#include "systemc.h"
#include <iostream>
#include "interface_stimulus.hpp"
#include "control_unit.hpp"
#include "multiplexer_2.hpp"
#include "multiplexer_3.hpp"
#include "multiplexer_3_2.hpp"
#include "register_file.hpp"
#include "adder.hpp"
#include "program_counter.hpp"




int sc_main (int argc, char* argv[]) {
	sc_clock clock("clock");
	sc_signal<sc_bv<1> > eq;
	sc_signal<sc_bv<3> > op_code;
	sc_signal<sc_bv<1> > load;
	sc_signal<sc_bv<3> > r_a;
	sc_signal<sc_bv<3> > r_b;
	sc_signal<sc_bv<3> > tgt_addr;
	sc_signal<sc_bv<16> > data_mem_out;
	sc_signal<sc_bv<16> > alu_out;
	sc_signal<sc_bv<16> > p_c_plus_one;
	sc_signal<sc_bv<2> > mux_pc;
	sc_signal<sc_bv<1> > mux_alu1;
	sc_signal<sc_bv<1> > mux_alu2;
	sc_signal<sc_bv<2> > func_alu;
	sc_signal<sc_bv<1> > we_dmem;
	sc_signal<sc_bv<16> > src1;
	sc_signal<sc_bv<16> > src2;
	sc_signal<sc_bv<1> > we_rf;
	sc_signal<sc_bv<2> > mux_tgt;
	sc_signal<sc_bv<16> > tgt;
	sc_signal<sc_bv<1> > mux_rf;
	sc_signal<sc_bv<3> > mux_src2;
	sc_signal<sc_bv<16> > sign_ext7;
	sc_signal<sc_bv<16> > one;
	sc_signal<sc_bv<16> > beq_dest;
	sc_signal<sc_bv<16> > pc_in;
	sc_signal<sc_bv<16> > pc_out;
	sc_signal<bool> control_ready;
	sc_signal<bool> register_file_ready;

	Interface_Stimulus i1("Interface_Stimulus");

	i1.load(load);
	i1.clock(clock);
	i1.eq(eq);
	i1.op_code(op_code);
	i1.r_a(r_a);
	i1.r_b(r_b);
	i1.tgt_addr(tgt_addr);
	i1.data_mem_out(data_mem_out);
	i1.alu_out(alu_out);
	i1.mux_alu1(mux_alu1);
	i1.mux_alu2(mux_alu2);
	i1.func_alu(func_alu);
	i1.we_dmem(we_dmem);
	i1.src1(src1);
	i1.src2(src2);
	i1.sign_ext7(sign_ext7);
	i1.one(one);

	Control_Unit c1("Control_Unit");
	
	c1.load(load);
	c1.clock(clock);
	c1.eq(eq);
	c1.op_code(op_code);
	c1.mux_pc(mux_pc);
	c1.mux_alu1(mux_alu1);
	c1.mux_alu2(mux_alu2);
	c1.func_alu(func_alu);
	c1.we_dmem(we_dmem);
	c1.we_rf(we_rf);
	c1.mux_tgt(mux_tgt);
	c1.mux_rf(mux_rf);
	c1.control_ready(control_ready);
	
	Multiplexer_2 m21("Multiplexer_2");
	
	m21.clock(clock);
	m21.in1(r_a);
	m21.in2(tgt_addr);
	m21.sel(mux_rf);
	m21.result(mux_src2);

	Multiplexer_3 m31("Multiplexer_3");

	m31.clock(clock);
	m31.in1(data_mem_out);
	m31.in2(alu_out);
	m31.in3(p_c_plus_one);
	m31.sel(mux_tgt);
	m31.result(tgt);

	Multiplexer_3_2 m32("Multiplexer_3");

	m32.clock(clock);
	m32.in1(beq_dest);
	m32.in2(p_c_plus_one);
	m32.in3(alu_out);
	m32.sel(mux_pc);
	m32.result(pc_in);

	Register_File r1("Register_File");
	
	r1.clock(clock);
	r1.tgt_addr(r_a);
	r1.src1_addr(r_b);
	r1.src2_addr(mux_src2);
	r1.tgt(tgt);
	r1.we(we_rf);
	r1.src1(src1);
	r1.src2(src2);
	r1.control_ready(control_ready);
	r1.register_file_ready(register_file_ready);
	
	Program_Counter p1("Program_Counter");
	
	p1.clock(clock);
	p1.in_addr(pc_in);
	p1.out_addr(pc_out);
	p1.control_ready(control_ready);
	
	Adder a1("Adder");
	
	a1.a(one);
	a1.b(pc_out);
	a1.s(p_c_plus_one);

	Adder a2("Adder");
	
	a2.a(sign_ext7);
	a2.b(p_c_plus_one);
	a2.s(beq_dest);


	//From now on I'm simulating each operation allowed

	//Open VCD file
	sc_trace_file *wf = sc_create_vcd_trace_file("prova");

	//Dump the desired signals
	sc_trace(wf, eq, "eq");
	sc_trace(wf, clock, "clock");
	sc_trace(wf, op_code, "op_code");
	sc_trace(wf, load, "load");
	sc_trace(wf, r_a, "r_a");
	sc_trace(wf, r_b, "r_b");
	sc_trace(wf, tgt_addr, "tgt_addr");
	sc_trace(wf, data_mem_out, "data_mem_out");
	sc_trace(wf, alu_out, "alu_out");
	sc_trace(wf, p_c_plus_one, "p_c_plus_one");
	sc_trace(wf, mux_pc, "mux_pc");
	sc_trace(wf, pc_out, "pc_out");
	sc_trace(wf, register_file_ready, "register_file_ready");
	sc_trace(wf, mux_alu1, "mux_alu1");
	sc_trace(wf, mux_alu2, "mux_alu2");
	sc_trace(wf, func_alu, "func_alu");
	sc_trace(wf, we_dmem, "we_dmem");
	sc_trace(wf, src1, "src1");
	sc_trace(wf, src2, "src2");
	sc_trace(wf, we_rf, "we_rf");
	sc_trace(wf, mux_tgt, "mux_tgt");
	sc_trace(wf, tgt, "tgt");
	sc_trace(wf, mux_rf, "mux_rf");	
	sc_trace(wf, mux_src2, "mux_src2");
	sc_trace(wf, pc_in, "pc_in");
	sc_trace(wf, control_ready, "control_ready");


	sc_start(SC_ZERO_TIME);
	sc_start(100, SC_NS);

	sc_close_vcd_trace_file(wf);
	return 0;

}
