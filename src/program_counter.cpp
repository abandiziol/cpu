#include "program_counter.hpp"

using namespace std;

	void Program_Counter::ReadAddr()
	{
		if (control_ready&(lock==2)) {
			out_addr = in_addr.read();

			cout << "Input of Program Counter is " << in_addr << endl;
	
			cout << "Output of Program Counter is " << out_addr << endl;
	
			lock = 0;
		} else {
			if (control_ready&(lock == 1)) {
				lock = 2;
			} else {
				if (control_ready&(lock == 0)) {
					lock =1;
				} else {
					lock = 0;
				}
			}
		}
		
		
		return;
	}
