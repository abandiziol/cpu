#include "interface_stimulus.hpp"

using namespace std;


void Interface_Stimulus::stimgen(){
	while(true){		

		load = 1;
		eq.write(1);
		op_code = "000";
		r_a = "010";
		r_b = "110";
		tgt_addr = "011";
		data_mem_out = "0101010101010101";
		alu_out = "1010101010101010";
		one = "0000000000000001";
		sign_ext7 = "1111111100000011";
		
		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait();

		load = 0;

		wait(SC_ZERO_TIME);
	
		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait(5, SC_NS);

		load = 1;
		eq.write(1);
		op_code = "001";
		r_a = "001";
		r_b = "111";
		tgt_addr = "011";
		data_mem_out = "1101010101010101";
		alu_out = "1010101011101010";
		sign_ext7 = "1111111100000011";

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait();

		load = 0;

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait(5, SC_NS);

		load = 1;
		eq.write(1);
		op_code = "010";
		r_a = "110";
		r_b = "010";
		tgt_addr = "011";
		data_mem_out = "0101011101011101";
		alu_out = "1010101010101010";
		sign_ext7 = "1111111100000011";
	
		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait();

		load = 0;

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait(5, SC_NS);
	
		load = 1;
		eq.write(1);
		op_code = "011";
		r_a = "000";
		r_b = "111";
		tgt_addr = "001";
		data_mem_out = "0101110101010101";
		alu_out = "1011101010101010";
		sign_ext7 = "1111111100000001";

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait();

		load = 0;

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait(5, SC_NS);

		load = 1;
		eq.write(1);
		op_code = "100";
		r_a = "110";
		r_b = "000";
		tgt_addr = "001";
		data_mem_out = "0101000101010111";
		alu_out = "1000101010101010";
		sign_ext7 = "1111111100000001";

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait();

		load = 0;

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait(5, SC_NS);

		load = 1;
		eq.write(1);
		op_code = "101";
		r_a = "000";
		r_b = "001";
		tgt_addr = "010";
		data_mem_out = "0111010101110101";
		alu_out = "1010001010101010";
		sign_ext7 = "1111111100000010";

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait();

		load = 0;

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;
		wait(5, SC_NS);

		load = 1;
		eq.write(1);
		op_code = "110";
		r_a = "111";
		r_b = "010";
		tgt_addr = "011";
		data_mem_out = "0101011101010101";
		alu_out = "1010101010101011";

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait();

		load = 0;

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait(5, SC_NS);

		load = 1;
		eq.write(1);
		op_code = "111";
		r_a = "111";
		r_b = "110";
		tgt_addr = "011";
		data_mem_out = "0101110101010101";
		alu_out = "1010101010100010";

		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait();

		load = 0;
	
		wait(SC_ZERO_TIME);

		cout << "Interface_stimulus @ " << sc_time_stamp() << endl;
		cout << "	load = " << load << endl;
		cout << "	eq = " << eq << endl;
		cout << "	op_code = " << op_code << endl;
		cout << "	r_a = " << r_a << endl;
		cout << "	r_b = " << r_b << endl;
		cout << "	tgt_addr = " << tgt_addr << endl;
		cout << "	data_mem_out = " << data_mem_out << endl;
		cout << "	alu_out = " << alu_out << endl;

		wait(5, SC_NS);

		return;
	}

}
