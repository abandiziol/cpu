// multiplexer

#include "multiplexer_2.hpp"

using namespace std;


void Multiplexer_2::do_selection()
{
	sc_bv<3> temp;
   	sc_bv<1> sel_signal = sel.read();

	cout << "Source 2 address multiplexer @ " << sc_time_stamp() << endl;
	cout << "	Input sel_signal = " << sel_signal << "(0 = r_c, 1 = r_a)" << endl;

   	if( sel_signal == '0' )
   	{
      		temp = in2.read();
		cout << "	r_c selected as source 2 address" << endl;
   	}
   	else
   	{
      		temp = in1.read();
		cout << "	r_a selected as source 2 address" << endl;
   	}

   	result.write(temp);
}
