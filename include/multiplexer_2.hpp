#ifndef MUX_2_HPP
#define MUX_2_HPP

#include <systemc.h>

using namespace std;

SC_MODULE(Multiplexer_2)
{
	sc_in<bool> clock;
   	sc_in<sc_bv<3> > in1;
   	sc_in<sc_bv<3> > in2;
   	sc_in<sc_bv<1> > sel;
   	sc_out<sc_bv<3> > result;

   	void do_selection();

   	SC_CTOR(Multiplexer_2) {
      		SC_METHOD(do_selection); 
      		sensitive_pos << clock; 
		dont_initialize();
   	}

};

#endif
