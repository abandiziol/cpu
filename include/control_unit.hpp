#ifndef CONTROL_HPP
#define CONTROL_HPP

//Here we design the control unit, which must decode the opcode and decide which will
//be the outputs to control all other units.

#include <systemc.h>
#include <iostream>

SC_MODULE(Control_Unit){

//For the structure of the Control Unit, please refer to the document "Risc-16 
//Sequential Implementation", that can be found at http://www.eng.umd.edu/~blj/RiSC/.
//The structure we are going to implement can be easily deduced from the one reported
//at pag.10.

	//Declaration of the inputs
	sc_in<bool> clock;
	sc_in<sc_bv<1> > eq;
	sc_in<sc_bv<3> > op_code;
	sc_in<sc_bv<1> > load;

	//Declaration of the outputs
	sc_out<sc_bv<2> > mux_pc;
	sc_out<sc_bv<1> > mux_alu1;
	sc_out<sc_bv<1> > mux_alu2;
	sc_out<sc_bv<2> > func_alu;
	sc_out<sc_bv<1> > we_dmem;
	sc_out<sc_bv<1> > we_rf;
	sc_out<sc_bv<2> > mux_tgt;
	sc_out<sc_bv<1> > mux_rf;
	sc_out<bool> control_ready;

	//Internal Variables
	sc_bv<3> op_code_int;

	//Methods Declaration
	void Decode();

	//Constructor
	SC_CTOR(Control_Unit) {

		SC_METHOD(Decode);
			sensitive_pos << clock;
			dont_initialize();
	}	

};//End of the Control Unit Module

#endif
